import React, { Component } from 'react';
import './App.css';
import Validation from './Validation/Validation';
import Char from './Char/Char';

class App extends Component {

  state = {
    inputText: '',
    inputTextLength: 0,
    inputTextLettersArray: []
  };

  onChangeTextHandler(event) {
    const changedText = event.target.value;
    const changedTextLength = changedText.length;
    const changedTextLettersArray = changedText.split('');

    this.setState({ inputText: changedText });
    this.setState({ inputTextLength: changedTextLength });
    this.setState({ inputTextLettersArray: changedTextLettersArray });

  }

  deleteLetterHandler = (letterIndex) => {
    const lettersArray = [...this.state.inputTextLettersArray];
    lettersArray.splice(letterIndex, 1);
    //convert array back to string
    const changedInputText = lettersArray.join('');
    this.setState({
      inputText: changedInputText,
      inputTextLettersArray: lettersArray
    });
  }

  render() {

    let charactersList = null;

    if (this.state.inputTextLength > 0) {
      charactersList = (
        <div>
          {this.state.inputTextLettersArray.map((letter, index) => {
            return <Char
              letter={letter}
              click={() => this.deleteLetterHandler(index)}
            />
          })}
        </div>
      );
    }

    return (
      <div className="App">
        <ol>
          <li>Create an input field (in App component) with a change listener which outputs the length of the entered text below it (e.g. in a paragraph).</li>
          <li>Create a new component (=> ValidationComponent) which receives the text length as a prop</li>
          <li>Inside the ValidationComponent, either output "Text too short" or "Text long enough" depending on the text length (e.g. take 5 as a minimum length)</li>
          <li>Create another component (=> CharComponent) and style it as an inline box (=> display: inline-block, padding: 16px, text-align: center, margin: 16px, border: 1px solid black).</li>
          <li>Render a list of CharComponents where each CharComponent receives a different letter of the entered text (in the initial input field) as a prop.</li>
          <li>When you click a CharComponent, it should be removed from the entered text.</li>
        </ol>
        <p>Hint: Keep in mind that JavaScript strings are basically arrays!</p>
        <input 
          type="text" 
          value={this.state.inputText} 
          onChange={(event) => this.onChangeTextHandler(event)} />
        <p>Text Length: {this.state.inputText.length}</p>
        <Validation textLength={this.state.inputTextLength} />
        {charactersList}
      </div>
    );
  }
}

export default App;
