import React from 'react';

const validation = (props) => {

    let textLengthMessage = "";

    if (props.textLength < 5) {
        textLengthMessage = "Text too short";
    } else {
        textLengthMessage = "Text long enough";
    }
    return (
        <div className="ValidationComponent">
            <p>{textLengthMessage}</p>
        </div>
    )
};

export default validation;